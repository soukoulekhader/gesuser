<?php

namespace Khader\Gesuser\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $libelle
 * @property string $module
 * @property string $action
 * @property string $params
 */
class Privilege extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'privilege';

    /**
     * @var array
     */
    protected $fillable = ['libelle', 'module', 'action', 'params'];

    public function role() {
        return $this->belongsToMany(Role::class, 'ligne_privilege', 'role_id', 'privilege_id');
    }

}
