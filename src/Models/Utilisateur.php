<?php

namespace Khader\Gesuser\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $role_id
 * @property string $nom
 * @property string $prenoms
 * @property string $username
 * @property string $date_nais
 * @property string $adresse
 * @property string $tel
 * @property string $email
 * @property string $password
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $remember_token
 * @property boolean $valide
 */
class Utilisateur extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'utilisateur';

    protected $dates = [
        'date_nais'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'nom', 
        'prenoms', 
        'username', 
        'date_nais', 
        'adresse', 
        'tel', 
        'email', 
        'password', 
        'image', 
        'valide',
        'role_id'
    ];

    public function role() {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

}
