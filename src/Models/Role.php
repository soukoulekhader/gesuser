<?php

namespace Khader\Gesuser\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $libelle
 */
class Role extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'role';

    /**
     * @var array
     */
    protected $fillable = ['libelle'];

    public function privileges() {
        return $this->belongsToMany(Privilege::class, 'ligne_privilege', 'privilege_id', 'role_id');
    }

    public function utilisateurs() {
        return $this->hasMany(Utilisateur::class, 'role_id', 'id');
    }

}
