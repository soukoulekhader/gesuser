<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrivilegeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('privilege', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('libelle')->nullable();
			$table->string('module', 100)->nullable();
			$table->text('action', 65535)->nullable();
			$table->string('params')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('privilege');
	}

}
