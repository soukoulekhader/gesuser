<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUtilisateurTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('utilisateur', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nom', 30)->nullable();
			$table->string('prenoms', 50)->nullable();
			$table->string('username', 50)->nullable();
			$table->date('date_nais')->nullable();
			$table->string('adresse', 30)->nullable();
			$table->string('tel', 30)->nullable();
			$table->string('email', 50);
			$table->string('password')->nullable();
			$table->string('image')->nullable()->default('user_default.jpg');
			$table->timestamps();
			$table->softDeletes();
			$table->string('remember_token', 100)->nullable();
			$table->boolean('valide');
			$table->integer('role_id')->unsigned()->index('fk_administrateur_role1_idx');
			$table->primary(['id','role_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('utilisateur');
	}

}
